// Loads Gulp Modules
var gulp = require('gulp');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var order = require('gulp-order');

// Compress and minify CSS
gulp.task('css', function(){
    gulp.src([
            'css/fonts.css',
            'css/style.css',
            'css/responsive.css'
        ])
        .pipe(order([
            'css/fonts.css',
            'css/style.css',
            'css/responsive.css'
        ], { base: './' }))
        .pipe(cleanCSS())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('dist/css'))
});

// Compress and minify JS
gulp.task('js', function(){
    gulp.src([
            'js/youtube.js',
            'js/app.js'
        ])
        .pipe(order([
            'js/youtube.js',
            'js/app.js'
        ], { base: './' }))
        .pipe(uglify())
        .pipe(concat('script.min.js'))
        .pipe(gulp.dest('dist/js'))
});

// Copy fonts
gulp.task('copy', function(){
    gulp.src([
            'fonts/*',
            '/'
        ], { base: './' })
        .pipe(gulp.dest('dist'))
});

gulp.task('default', gulp.parallel('css', 'js', 'copy'));