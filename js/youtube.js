'use strict';

var maxResults = 20,
    apiKey = 'AIzaSyDVvQTCYIeI6l4TY1Z7b3-KAu8iiXJm9Cc',
    part = 'snippet';


/**
 *  Get YoutTube Playlist
 * 
 *  @param string playlist = YouTube Playlist ID
 * 
 *  @return void
 *
 */
function getPlaylist(playlistId) {
    
    // YouTube API Playlist Endpoint
    var apiEndpoint = 'https://www.googleapis.com/youtube/v3/playlists?part=' + part + '&maxResults=' + maxResults + '&key=' + apiKey + '&id=' + playlistId;
    
    getApiData(apiEndpoint, playlistId, generatePlaylistDetails);
}


/**
 *  Generate YoutTube API Data
 * 
 *  @param string requestUrl = YouTube API Endpoint
 *  @param string playlistId = YouTube Playlist ID
 *  @param function callback = callback function
 * 
 *  @return void
 *
 */
function getApiData(requestUrl, playlistId, callback) {
    
    var request = new XMLHttpRequest();
    
    request.onreadystatechange = function() {
        if (request.readyState == XMLHttpRequest.DONE) {
            
            if(request.status == '200') {
                
                // Execute Callback
                callback(request.response, playlistId);
                
            }
            else {
                
                // Show Error Message
                alert('Error ' + request.status + '!\n' + 'Please check playlist ID and try again');
            }
        }
    };
    
    request.open('GET', requestUrl, true);
    request.send();
    
}


/**
 *  Generate YoutTube Playlist Details
 * 
 *  @param JSON data = YouTube JSON response
 *  @param string playlistId = YouTube Playlist ID
 * 
 *  @return void
 *
 */
function generatePlaylistDetails(data, playlistId) {
    
    if(data.length) {
        
        data = JSON.parse(data);
        
        document.getElementById('playlist-details-wrapper').innerHTML = '';
        
        // Create elements
        var playlistDetails = document.createElement('div'),
            playlistTitle = document.createElement('h2'),
            playlistDesc = document.createElement('div'),
            imageWrapper = document.createElement('div'),
            image = document.createElement('img'),
            link = document.createElement('a'),
            detailsWrapper = document.getElementById('playlist-details-wrapper');
        
        detailsWrapper.appendChild(imageWrapper);
        detailsWrapper.appendChild(playlistDetails);
        detailsWrapper.appendChild(link);
        playlistDetails.className = 'playlist-details';        
        playlistDetails.appendChild(playlistTitle);
        playlistDetails.appendChild(playlistDesc);
        imageWrapper.appendChild(image);
        imageWrapper.className = 'image-wrapper';
        link.className = 'btn';
        link.setAttribute('target', '_blank');
        link.innerHTML = 'Watch on YouTube';

        // Set elements properties
        image.src = data.items[0].snippet.thumbnails.high.url;
        playlistTitle.innerHTML = data.items[0].snippet.channelTitle;
        playlistDesc.innerHTML = data.items[0].snippet.description;
        link.href = 'https://www.youtube.com/playlist?list=' + playlistId;
        
        // YouTube API Playlist Items Endpoint
        var requestUrl = 'https://www.googleapis.com/youtube/v3/playlistItems?part=' + part + '&maxResults=' + maxResults + '&key=' + apiKey + '&playlistId=' + playlistId;
        
        // Get playlist items
        getApiData(requestUrl, playlistId, generatePlaylist);
    }
    else {
        
        // No Results Message
        if( ! document.getElementById('no-results')) {
            var noResults = document.createElement('div');
            noResults.id = 'no-results';
            noResults.innerHTML = 'Playlist is empty';
            document.getElementById('playlist').appendChild(noResults);
        }
    }
}


/**
 *  Generate YoutTube Playlist
 * 
 *  @param JSON playlist = YouTube JSON response
 * 
 *  @return void
 *
 */
function generatePlaylist(playlist) {
    
    playlist = JSON.parse(playlist);
    
    if(playlist.items.length) {

        // Empty playlist
        document.getElementById('playlist').innerHTML = '';

        // Insert videos
        playlist.items.forEach(function(item, index) {

            var itemElement = document.createElement('div'),
                iframeWrapper = document.createElement('div'),
                iframe = document.createElement('iframe'),
                videoTitle = document.createElement('h3'),
                videoDesc = document.createElement('div');

            // Create playlist item
            itemElement.classList = 'playlist-item';
            document.getElementById('playlist').appendChild(itemElement);

            // Create iFrame responsive wrapper
            iframeWrapper.classList = 'iframe-wrapper';
            itemElement.appendChild(iframeWrapper);

            // Create iFrame
            iframe.src = 'https://www.youtube.com/embed/' + item.snippet.resourceId.videoId;
            iframe.setAttribute('frameborder', 0);
            iframe.setAttribute('allow', 'autoplay; encrypted-media');
            iframe.setAttribute('allowfullscreen', '');
            iframeWrapper.appendChild(iframe);

            // Create video title
            videoTitle.classList = 'playlist-item-title';
            videoTitle.innerHTML = item.snippet.title;
            itemElement.appendChild(videoTitle);

            // Create video description
            videoDesc.classList = 'playlist-item-title';
            videoDesc.innerHTML = item.snippet.description;
            itemElement.appendChild(videoDesc);

        });
    }
    else {
        
        // No Results Message
        if( ! document.getElementById('no-results')) {
            var noResults = document.createElement('div');
            noResults.id = 'no-results';
            noResults.classList = 'no-results';
            noResults.innerHTML = 'Playlist is empty';
            document.getElementById('playlist').appendChild(noResults);
        }
    }
}